<?php declare(strict_types = 1);

namespace Lister\Vars;

/**
 * Class Strings
 *
 * Strings helpers
 */
class Strings
{

    /**
     * Is it ASCII?
     * @param string $text
     * @return bool
     */
    public static function isAscii(string $text): bool
    {
        // Printable only
        return mb_check_encoding($text, 'ASCII');
    }

    /**
     * Is it UTF8?
     * @param string $text
     * @return bool
     */
    public static function isUtf8(string $text): bool
    {
        return mb_check_encoding($text, 'UTF-8');
    }

    /**
     * Is it multibyte string?
     * @param string $text
     * @return bool
     */
    public static function isMb(string $text): bool
    {
        return !mb_check_encoding($text, 'ASCII') && mb_check_encoding($text, 'UTF-8');
    }

    /**
     * Transliterate text to ASCII.
     * @param string $text
     * @param string $encoding
     * @return string
     */
    public static function ascii(string $text, string $encoding = 'UTF-8'): string
    {
        // Is it already pure ascii?
        if (static::isAscii($text))
            return $text;
        // Iconv implementation
        $glibc = 'glibc' === ICONV_IMPL;
        if ($glibc)
            return iconv($encoding, 'ASCII//TRANSLIT', $text);
        else {
            $pattern = '/[^\x00-\x7F]/' . (static::isMb($text) ? 'u' : null);
            $result = preg_replace_callback($pattern, function ($c) use ($encoding) {
                $t = iconv($encoding, 'ASCII//IGNORE//TRANSLIT', $c[0]);
                if (isset($t[1]))
                    $t = ltrim($t, '\'`"^~');
                return $t;
            }, $text);
            return $result;
        }
    }

    /**
     * Generate slug
     * @param string $text
     * @param string $encoding
     * @return string
     */
    public static function slug(string $text, string $encoding = 'UTF-8'): string
    {
        $text = $text = str_replace(' ', '-', trim(strip_tags($text)));
        $text = strtolower(static::ascii($text, $encoding));
        return preg_replace('/[^a-z0-9-_]+/', '', $text);
    }

    /**
     * Snake case
     * @param string $text
     * @param string $delimiter
     * @return string
     */
    public static function snake(string $text, string $delimiter = '_'): string
    {
        $replace = '$1' . $delimiter . '$2';
        return ctype_lower($text) ? $text : strtolower(preg_replace('/([A-Z]*[a-zA-Z1-9])([A-Z])/', $replace, $text));
    }

    /**
     * Studly case
     * @param string $text
     * @return string
     */
    public static function studly(string $text): string
    {
        $text = ucwords(str_replace(['-', '_'], ' ', $text));
        return str_replace(' ', '', $text);
    }

    /**
     * Camel case
     * @param string $text
     * @return string
     */
    public static function camel(string $text): string
    {
        return lcfirst(static::studly($text));
    }

    /**
     * Determine if a string starts with
     * @param string $haystack
     * @param $needles
     * @return bool
     */
    public static function startsWith(string $haystack, $needles): bool
    {
        foreach ((array)$needles as $needle) {
            if ($needle && strpos($haystack, $needle) === 0)
                return true;
        }
        return false;
    }

    /**
     * Determine if a string contains a substring
     * @param string $haystack
     * @param $needles
     * @return bool
     */
    public static function contains(string $haystack, $needles): bool
    {
        foreach ((array)$needles as $needle) {
            if ($needle && strpos($haystack, $needle) !== false)
                return true;
        }
        return false;
    }

    /**
     * Determine if a string ends with
     * @param string $haystack
     * @param $needles
     * @return bool
     */
    public static function endsWith(string $haystack, $needles): bool
    {
        foreach ((array)$needles as $needle) {
            if ($needle && $needle == substr($haystack, strlen($haystack) - strlen($needle)))
                return true;
        }
        return false;
    }

    /**
     * Compute and print percents
     * @param $cake
     * @param $piece
     * @param int $digits
     * @return string
     */
    public static function perc($cake, $piece, int $digits = 0): string
    {
        return sprintf('%.' . $digits . 'f', $piece / ($cake / 100));
    }

    /**
     * Format key/value and implode
     * @example Strings::implode('%s = "%s"', ',', $values, '%s = NULL');
     * @param string $format
     * @param string $glue
     * @param array $array
     * @param array|string $formatNull format nulls
     * @return string
     */
    static public function implode(string $format, string $glue, array $array = [], string $formatNull = null): string
    {
        $tmp = [];
        foreach ($array as $key => $value)
            $tmp[] = ($formatNull && $value === null)
                ? sprintf($formatNull, $key)
                : sprintf($format, $key, $value);
        return implode($glue, $tmp);
    }

    /**
     * Explode into array with specific number of pieces
     * @example list($a, $b, $c) = strings::explode(' ', 'aa bb', 3);
     * @param string $delimiter
     * @param string $text
     * @param int $count
     * @return array
     */
    static public function explode(string $delimiter, string $text, int $count): array
    {
        $result = explode($delimiter, $text, $count);
        if (($cr = count($result)) < $count) {
            $result = array_merge($result, array_fill($cr, $count - $cr, null));
        }
        return $result;
    }

    /**
     * Explode by regexp into array with specific number of pieces
     * @example list($a, $b, $c) = strings::explode('~\s*~', 'aa bb', 3);
     * @param string $delimiter
     * @param string $text
     * @param int $count
     * @return array
     */
    static public function prexplode(string $delimiter, string $text, int $count): array
    {
        $result = preg_split($delimiter, $text, $count);
        if (($cr = count($result)) < $count) {
            $result = array_merge($result, array_fill($cr, $count - $cr, null));
        }
        return $result;
    }

    /**
     * Limit the number of characters in a string
     * @param string $text
     * @param int $chars
     * @param string $cut
     * @param string $stopAt
     * @return string
     */
    public static function limit(string $text, int $chars, string $cut = '...', string $stopAt = ' '): string
    {
        if (strlen($text) <= $chars)
            return $text;
        else {
            if ($stopAt) {
                $text = substr($text, 0, $chars + strlen($stopAt));
                // If is stopAt at position 0 cut normally
                if ($found = strrpos($text, $stopAt))
                    $text = substr($text, 0, $found);
                else
                    $text = substr($text, 0, $chars);
            } else
                $text = substr($text, 0, $chars);
            return $text . $cut;
        }
    }

    /**
     * Random string
     * @param int $length
     * @param string $letters
     * @return string
     */
    public static function random(int $length = 10, string $letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'): string
    {
        $string = $letters = str_shuffle($letters);
        if ($repeat = (int)round($length / strlen($letters)))
            $string .= str_repeat($letters, $repeat);
        return substr(str_shuffle($string), 0, $length);
    }

    /**
     * Return only digits
     * @param string $text
     * @return string
     */
    public static function numero(string $text): string
    {
        return preg_replace('/[^\d]/m', '', $text);
    }

    /**
     * Same start
     * @param string $one
     * @param string $two
     * @return string
     */
    public static function sameStart(string $one, string $two): string
    {
        // XOR is performed char by char, so it returns \x00 for the same chars
        if ($pos = strspn($one ^ $two, "\x00"))
            return substr($one, 0, $pos);
    }

    /**
     * Same end
     * @param string $one
     * @param string $two
     * @return string
     */
    public static function sameEnd(string $one, string $two): string
    {
        // XOR is performed char by char, so it returns \x00 for the same chars
        // We must first reverse both strings before XOR because of UTF-8
        if ($pos = strspn(strrev($one) ^ strrev($two), "\x00")) {
            return substr($one, -$pos);
        }
    }

}