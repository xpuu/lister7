<?php declare(strict_types = 1);

namespace Lister\Vars;

use Exception;

/**
 * Class Json
 *
 * Helps to keep the same settings across the whole application
 */
class Json
{
    /**
     * Configuration
     * @var Data
     */
    public $config;

    /**
     * Json constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        // Config
        $this->config = new Data([
            // Defaults
            'depth' => 512,
            'flags' => JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK
        ], $config);
    }

    /**
     * Encode variable using global configuration
     * @param $var
     * @param int|null $flags
     * @param int|null $depth
     * @return string
     * @throws JsonException
     */
    public function encode($var, int $flags = null, int $depth = null): string
    {
        $c = $this->config;
        // Convert to integer and check (otherwise we risk Segmentation fault)
        $depth = (int)($depth ?? $c['depth']);
        $depth = $depth > 0 ? $depth : 0;
        // Encode
        $var = json_encode($var, $flags ?? $c['flags'], $depth);
        if ($error = json_last_error())
            throw new JsonException(json_last_error_msg(), $error);
        return $var;
    }

    /**
     * Decode string using global configuration
     * @param string $text
     * @param int|null $depth
     * @return array
     * @throws JsonException
     */
    public function decode(string $text, int $depth = null)
    {
        $c = $this->config;
        // Convert to integer and check (otherwise we risk Segmentation fault)
        $depth = (int)($depth ?? $c['depth']);
        $depth = $depth > 0 ? $depth : 1;
        // Decode
        $var = json_decode($text, true, $depth);
        if ($error = json_last_error())
            throw new JsonException(json_last_error_msg(), $error);
        return $var;
    }

}

## ---- Exceptions

/**
 * Class JsonException
 */
class JsonException extends Exception
{
}