<?php declare(strict_types = 1);

namespace Lister\Vars;

use TypeError;

/**
 * Class Arrays
 *
 * Arrays helpers
 */
class Arrays
{

    /**
     * Is array associative?
     * @param array $array
     * @return bool
     */
    public static function isAssoc(array $array): bool
    {
        $keys = array_keys($array);
        return array_keys($keys) !== $keys;
    }

    /**
     * Is array multidimensional?
     * @param array $array
     * @return bool
     */
    public static function isMulti(array $array): bool
    {
        return !(count($array) == count($array, COUNT_RECURSIVE));
    }

    /**
     * Multidimensional array_diff
     * @param array $first
     * @param array $second
     * @return array
     */
    public static function diffMulti(array $first, array $second): array
    {
        return array_udiff($first, $second, function ($a, $b) {
            if ($a === $b)
                return 0;
            elseif ($a > $b)
                return 1;
            else
                return -1;
        });
    }

    /**
     * Get a subset of the items from the given array
     * @param array $array
     * @param $keys
     * @return array
     */
    public static function only(array $array, $keys): array
    {
        return array_intersect_key($array, array_flip((array)$keys));
    }

    /**
     * Get all of the given array except for a specified array of items
     * @param array $array
     * @param $keys
     * @return array
     */
    public static function except(array $array, $keys): array
    {
        return array_diff_key($array, array_flip((array)$keys));
    }

    /**
     * Recursive array_merge
     * @param array $args
     * @return array
     * @throws TypeError
     */
    public static function merge(...$args): array
    {
        $result = [];
        // Overwrite char?
        $ochar = false;
        if (is_string($last = end($args)) && strlen($last) == 1)
            $ochar = array_pop($args);
        // Merge
        foreach ($args as $i => $array) {
            if (!is_array($array)) {
                $msg = 'Argument #%s passed to %s() must be of the type array, %s given';
                throw new TypeError(sprintf($msg, ($i + 1), __METHOD__, gettype($array)));
            }
            if (!$result)
                $result = $array;
            elseif (!static::isAssoc($array)) {
                // Indexed arrays are merged without duplicates
                foreach ($array as $value)
                    if (!in_array($value, $result, true))
                        $result[] = $value;
            } else {
                // Associative array
                foreach ($array as $key => $value) {
                    // Does key begin with overwrite char?
                    $overwrite = false;
                    if ($ochar && is_string($key) && $key{0} === $ochar) {
                        $key = substr($key, 1);
                        $overwrite = true;
                    }
                    // Key doesn't exist in original / Overwrite instead of merge
                    if (!array_key_exists($key, $result) || $overwrite
                        // Old / new value is not array
                        || !(is_array($value) && is_array($result[$key]))) {
                        $result[$key] = $value;
                    } else {
                        // Arrays are merged recursively
                        $result[$key] = $ochar
                            ? static::merge($result[$key], $value, $ochar)
                            : static::merge($result[$key], $value);
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Get an array key using dot notation
     * @param array $array
     * @param string $path
     * @param null $default
     * @return mixed
     */
    public static function get(array $array, $path, $default = null)
    {
        if (!is_string($path) || !$path)
            return $array[$path] ?? $default;
        else {
            // Options
            foreach ([':' => 'forceArray', '?' => 'exists'] as $opt => $flag) {
                if ($$flag = ($path{-1} == $opt))
                    $path = substr($path, 0, -1);
            }
            // No path
            if (isset($array[$path]))
                return $exists ? true : ($forceArray ? (array)$array[$path] : $array[$path]);
            // Explode path
            foreach (explode('.', $path) as $segment) {
                if (!is_array($array) || !array_key_exists($segment, $array))
                    return $exists ? false : ($forceArray ? (array)$default : $default);
                $array = $array[$segment];
            }
            return $exists ? true : ($forceArray ? (array)$array : $array);
        }

    }

    /**
     * Set an array key using dot notation
     * @param array $array
     * @param string $path
     * @param $value
     */
    public static function set(array &$array, $path, $value): void
    {
        if (!is_string($path) || !$path)
            $array[$path] = $value;
        else {
            // Options
            $last = $path{-1};
            foreach (['>' => 'push', '<' => 'unshift'] as $opt => $flag) {
                if ($$flag = ($last == $opt))
                    $path = substr($path, 0, -1);
            }
            // Explode path
            $key = $path;
            if (strpos($path, '.') !== false) {
                $segments = explode('.', $path);
                $key = array_pop($segments);
                foreach ($segments as $i) {
                    if (!array_key_exists($i, $array) || !is_array($array[$i]))
                        $array[$i] = [];
                    $array = &$array[$i];
                }
            }
            // Set
            if ($push || $unshift) {
                if (!array_key_exists($key, $array) || !is_array($array[$key]))
                    $array[$key] = [$value];
                elseif ($push)
                    $array[$key][] = $value;
                elseif ($unshift)
                    array_unshift($array[$key], $value);
            } else
                $array[$key] = $value;
        }
    }

    /**
     * Unset an array key using dot notation
     * @param array $array
     * @param $path
     */
    public static function unset(array &$array, $path): void
    {
        // No path
        if (isset($array[$path]) || !is_string($path) || !$path)
            unset($array[$path]);
        else {
            // Explode path
            $key = $path;
            if (strpos($path, '.') !== false) {
                $segments = explode('.', $path);
                $key = array_pop($segments);
                foreach ($segments as $i) {
                    if (!array_key_exists($i, $array) || !is_array($array[$i]))
                        return;
                    $array = &$array[$i];
                }
            }
            // Unset
            unset($array[$key]);
        }
    }


    ## ---- Filters

    /**
     * Removes null values only
     * @param $value
     * @return bool
     */
    public static function filterRemoveNull($value): bool
    {
        return !($value === null);
    }

    /**
     * Recursive array filter
     * @param array $array
     * @param callable|null $callback
     * @param int $flags ARRAY_FILTER_USE_KEY, ARRAY_FILTER_USE_BOTH
     * @return array
     */
    public static function filter(array $array, callable $callback = null, int $flags = 0): array
    {
        $callback = $callback ?? 'static::filterRemoveNull';
        if (static::isMulti($array)) {
            while ([$key, $value] = each($array))
                if (is_array($value))
                    $array[$key] = static::filter($value, $callback, $flags);
        }
        return array_filter($array, $callback, $flags);
    }

}