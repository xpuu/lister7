<?php declare(strict_types = 1);

namespace Lister\Vars;

use ArrayAccess;

/**
 * Class Fluent
 *
 * Fluent parameter setting syntax
 */
class Fluent implements ArrayAccess
{

    /**
     * Attributes
     * @var array
     */
    protected $attributes;

    /**
     * Fluent constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * Fluent params set
     * @param string $method
     * @param array $params
     * @return Fluent
     */
    public function __call(string $method, array $params): Fluent
    {
        $this->attributes[$method] = count($params) > 0 ? $params[0] : true;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    ## ---- ArrayAccess

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return isset($this->attributes[$offset]);
    }

    /**
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->attributes[$offset] ?? null;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value): void
    {
        $this->attributes[$offset] = $value;
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset): void
    {
        unset($this->attributes[$offset]);
    }

}