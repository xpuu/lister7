<?php declare(strict_types=1);

namespace Lister\Vars;

use Iterator, ArrayAccess;

/**
 * Class DataProxy
 *
 * Proxy bound to specific key in parent Data object
 */
class DataProxy implements Iterator, ArrayAccess
{
    /**
     * @var Data|null $source Source Data object
     */
    protected $source = null;

    /**
     * @var string|null $bind Key bound in source object
     */
    public $bind = null;

    /**
     * DataProxy constructor.
     * @param Data $source
     * @param mixed $bind
     */
    public function __construct(Data $source, $bind)
    {
        $this->source = $source;
        $this->bind = $bind;
    }

    /**
     * Get new bound object
     * @param $key
     * @return DataProxy
     */
    public function bind($key): DataProxy
    {
        return $this->source->bind($this->formatKey($key));
    }

    /**
     * Assign variables to bound key
     * @param array $array
     */
    public function assign(array $array = []): void
    {
        $this->source->assign($this->bind, $array);
    }

    /**
     * Default variables to bound key
     * @param array $array
     */
    public function defaults(array $array): void
    {
        $this->source->defaults($this->bind, $array);
    }

    /**
     * Setup variables to bound key
     * @param \array[] ...$args
     * @return mixed
     */
    public function setup(array ...$args)
    {
        array_unshift($args, $this->bind);
        return call_user_func_array([$this->source, 'setup'], $args);
    }

    ## ---- Helper methods

    /**
     * Format key
     * @param string $key
     * @return string
     */
    protected function formatKey($key): string
    {
        $global = false;
        if ($key && is_string($key) && ($global = (substr($key, 0, 1) == '#')))
            $key = substr($key, 1);
        return $global ? $key : "{$this->bind}.{$key}";
    }

    /**
     * Export to string
     * @return string
     */
    public function asString(): string
    {
        return $this->source->asString($this->bind);
    }

    /**
     * Magic method for string conversion
     * @return string
     */
    public function __toString(): string
    {
        return $this->source->asString($this->bind);
    }

    ## ---- Array access

    /**
     * @param mixed $key
     * @return bool
     */
    public function offsetExists($key): bool
    {
        return true;
    }

    /**
     * @param mixed $key
     * @return mixed
     */
    public function offsetGet($key)
    {
        return $this->source->offsetGet($this->formatKey($key));
    }

    /**
     * @param mixed $key
     * @param mixed $value
     */
    public function offsetSet($key, $value): void
    {
        $this->source->offsetSet($this->formatKey($key), $value);
    }

    /**
     * @param mixed $key
     */
    public function offsetUnset($key): void
    {
        $this->source->offsetUnset($this->formatKey($key));
    }

    ## ---- Iterator

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->source->current($this->bind);
    }

    /**
     * @return mixed
     */
    public function next()
    {
        return $this->source->next($this->bind);
    }

    /**
     * @return mixed
     */
    public function key()
    {
        return $this->source->key($this->bind);
    }

    /**
     * @return mixed
     */
    public function rewind()
    {
        return $this->source->rewind($this->bind);
    }

    /**
     * @return bool
     */
    public function valid(): bool
    {
        return $this->source->valid($this->bind);
    }
}


/**
 * Class Data
 *
 * Intelligent array
 */
class Data implements Iterator, ArrayAccess
{

    /**
     * @var string $proxy Proxy class name
     */
    protected $proxy = 'Lister\Vars\DataProxy';

    /**
     * @var array $data Internal data array
     */
    protected $data = [];

    /**
     * Data constructor.
     * @param \array[] ...$args
     */
    public function __construct(array ...$args)
    {
        call_user_func_array([$this, 'setup'], $args);
    }

    /**
     * Return proxy object bound to specific key
     * @param $key
     * @return mixed
     */
    public function bind($key): DataProxy
    {
        switch (true) {
            case is_bool($key) || is_numeric($key):
                $key = (int)$key;
                break;
            case is_array($key) || is_object($key):
                trigger_error('Illegal offset type', E_USER_WARNING);
                $key = '';
                break;
            default:
                $key = (string)$key;
        }
        return new $this->proxy($this, $key);
    }

    /**
     * Assign array to internal data array
     * @param array ...$args
     */
    public function assign(...$args): void
    {
        $bind = (isset($args[0]) && is_scalar($args[0])) ? array_shift($args) : false;
        $args[0] = $args[0] ?? [];
        if ($bind)
            Arrays::set($this->data, $bind, $args[0]);
        else
            $this->data = $args[0];
    }

    /**
     * Default variables to internal data array
     * @param array ...$args
     */
    public function defaults(...$args): void
    {
        $bind = (isset($args[0]) && is_scalar($args[0])) ? array_shift($args) : false;
        if (!isset($args[0]))
            return;
        $target = $bind ? Arrays::get($this->data, "{$bind}:") : $this->data;
        $setup = $target ? Arrays::merge($args[0], $target) : $args[0];
        if ($bind)
            Arrays::set($this->data, $bind, $setup);
        else
            $this->data = $setup;
    }

    /**
     * Merge variables into internal data array or return it's content
     * @param array ...$args
     * @return array|mixed
     */
    public function setup(...$args)
    {
        if (!$args)
            return $this->data;
        else {
            $bind = is_scalar($args[0]) ? array_shift($args) : false;
            $value = $bind !== false ? Arrays::get($this->data, "{$bind}:") : $this->data;
            switch (count($args)) {
                case 0:
                    return $value;
                case 1:
                    $setup = $value ? Arrays::merge($value, $args[0]) : $args[0];
                    break;
                case 2:
                    $setup = $value ? Arrays::merge($value, $args[0], $args[1]) : Arrays::merge($args[0], $args[1]);
                    break;
                default:
                    array_unshift($args, $value);
                    $setup = call_user_func_array('Lister\Vars\Arrays::merge', $args);
            }
            if ($bind)
                Arrays::set($this->data, $bind, $setup);
            else
                $this->data = $setup;
        }
    }

    ## ---- Helper methods

    /**
     * Export to string
     * @param null $bind
     * @return string
     */
    public function asString($bind = null): string
    {
        return var_export($bind ? Arrays::get($this->data, "{$bind}:") : $this->data, true);
    }

    /**
     * Magic method for string conversion
     * @return string
     */
    public function __toString(): string
    {
        return $this->asString();
    }

    ## ---- Array Access

    /**
     * @param mixed $key
     * @return bool
     */
    public function offsetExists($key): bool
    {
        return true;
    }

    /**
     * @param mixed $key
     * @return mixed
     */
    public function offsetGet($key)
    {
        return Arrays::get($this->data, $key);
    }

    /**
     * @param mixed $key
     * @param mixed $value
     */
    public function offsetSet($key, $value): void
    {
        Arrays::set($this->data, $key, $value);
    }

    /**
     * @param mixed $key
     */
    public function offsetUnset($key): void
    {
        Arrays::unset($this->data, $key);
    }

    ## ---- Iterator

    /**
     * Helper function
     * @param mixed $path
     * @return array
     */
    protected function &getBindedArray($path): array
    {
        static $array = [];
        $data = &$this->data;
        foreach (explode('.', $path) as $segment) {
            if (!is_array($data) || !array_key_exists($segment, $data))
                return $array;
            $data = &$data[$segment];
        }
        return $data;
    }

    /**
     * @param null $bind
     * @return mixed
     */
    public function current($bind = null)
    {
        if (!$bind)
            return current($this->data);
        else {
            $data = &$this->getBindedArray($bind);
            return current($data);
        }
    }

    /**
     * @param null $bind
     * @return mixed
     */
    public function key($bind = null)
    {
        if (!$bind)
            return key($this->data);
        else {
            $data = &$this->getBindedArray($bind);
            return key($data);
        }
    }

    /**
     * @param null $bind
     * @return mixed
     */
    public function next($bind = null)
    {
        if (!$bind)
            return next($this->data);
        else {
            $data = &$this->getBindedArray($bind);
            return next($data);
        }
    }

    /**
     * @param null $bind
     * @return mixed
     */
    public function rewind($bind = null)
    {
        if (!$bind)
            return reset($this->data);
        else {
            $data = &$this->getBindedArray($bind);
            return reset($data);
        }
    }

    /**
     * @param string|null $bind
     * @return bool
     */
    public function valid(string $bind = null): bool
    {
        if (!$bind)
            return key($this->data) !== null;
        else {
            $data = &$this->getBindedArray($bind);
            return key($data) !== null;
        }
    }

}